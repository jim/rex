# -*- coding: utf-8 -*-
r"""
=========================================================
:mod:`git` --- GIT repository tools.
=========================================================

Sample Config File
[DEFAULT]
dir = lib

[Three20]
url = https://github.com/facebook/three20.git
branch = development

[MBProgressHUD]
url = https://github.com/jdg/MBProgressHUD.git

"""
import os.path
import configobj

from rex.core import regex
from rex.core import Object
from rex.core.logger import getLogger


__all__ = ['Module']


logger = getLogger(__name__)


class Module(Object):
    """GIT repos base submodule helpers.

    Attributes:
        * root: path of the root repository (MUST be the same as config file).
        * config: path of the configuration file.
    """
    def __init__(self, config):
        assert os.path.isfile(config), '<file: %s> is not valid' % config

        self.root = os.path.abspath(os.path.dirname(config))
        assert os.path.isdir(os.path.join(self.root, '.git')), \
                ('No valid root repository found under: %s' % self.root)

        self.config = configobj.ConfigObj(config)
        self.validate()


    def validate(self):
        """Validate configured submodules.
            * ensure :data:`url` is configured under each repos section.
            * check if the remote GIT repos is a valid URL address. 
        """
        for section in self.config.sections:
            assert ('url' in self.config[section]), \
                    ('<repos: %s> URL missing' % section)
            assert regex.url.match(self.config[section]['url']), \
                    ('<repos: %s> invalid repos URL found' % section)

    
    def fetch(self):
        """Fetch all configured repos as submodules.
        """
        pass


    def remove(self, section):
        """Remove the given section from root repository.
            * file: .gitmodules
            * file: .git/config
            * git rm --cached <path>'
            * submodule path from disk.

        Args:
            * section: section name.
        """
        # Ref. http://chrisjean.com/2009/04/20/git-submodules-adding-using-removing-and-updating/
        logger.warning('<module: {name}> removing...'.format(name=section))


    def reset(sel):
        """Remove all configured submodules from root repository.

        """
        pass


    def update(self):
        """Update all configured submodules.
        """
        pass

