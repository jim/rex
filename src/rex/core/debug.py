# -*- coding: utf-8 -*-
r"""
:mod:`debug` --- Common debug toolset. 
==========================================

"""
import inspect


__all__ = ['traceback']


def traceback(frame, parent=False):
    """Pick frame info from current caller's :py:data:`frame`.

    Args:
        * frame: :type:`frame` instance, use :func:`inspect.currentframe`.
        * parent: whether to get outer frame (caller) traceback info, :data:`False` by default.

    Returns:
        :class:`inspect.Trackback` instance from :data:`frame` or its parent frame.
    """
    # Traceback(filename='<stdin>', lineno=1, function='<module>', code_context=None, index=None)
    if parent is True:
        # frame itself will always be placed @ the first index of its outerframes.
        outers = inspect.getouterframes(frame)
        traceback = (len(outers) == 1) and None or inspect.getframeinfo(outers[1][0])
    else:
        traceback = inspect.getframeinfo(frame)
    return traceback
