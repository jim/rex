# -*- coding: utf-8 -*-
r"""
======================================================
:mod:`string` --- string related tools. 
======================================================

"""
import uuid
from rex.core import consts


__all__ = ['hex']


def hex(string):
    """Generate a :class:`uuid.UUID` for the :data:`string` & get its hex value.

    Internally use :func:`uuid.uuid5` for the given :data:`string` and
    :data:`uuid.NAMESPACE_DNS` as its namespace.

    Args:
        * string (str): seed string to be hexed.

    Returns:
        Hex string of the generated :class:`uuid.UUID` instance.
    """
    key = isinstance(string, consts.StringType) and string or str(string)
    return uuid.uuid5(uuid.NAMESPACE_OID, key).hex


