# -*- coding: utf-8 -*-
r"""
========================================================================
:mod:`config` --- Utilities for configuration files. 
========================================================================

"""
from __future__ import with_statement

import os.path

from rex.core import consts
from rex.core import system
from rex.core import logger as Logger


logger = Logger.getLogger()


__all__ = ['Config']


logger = Logger.getLogger()


class Config:
    """A :class:`ConfigParser` client.

    This class is provided for better access of :class:`ConfigParser`.

    Attributes:
        * path: absolute path of the configuration file.
    """
    def __init__(self, path):
        self.dirty = False
        self.parser = self._create()
        # NOTE <configparser.ConfigParser> at py2 need unicode's path.
        self.path = consts.UnicodeType(system.abspath(path))
        self.reload()


    def _create(self):
        try:
            from configparser import ConfigParser, ExtendedInterpolation
            return ConfigParser(interpolation=ExtendedInterpolation())
        except ImportError:
            logger.warn('<Fallback: ConfigParser.ConfigParser>, please install <package: configparser>')
        from ConfigParser import ConfigParser
        return ConfigParser()


    @property
    def defaults(self):
        """Return the default settings in :mod:`dict`."""
        return self.parser.defaults()


    @property
    def sections(self):
        """Return all sections in the config file expect DEFAULT."""
        return self.parser.sections()


    @property
    def rawdata(self):
        """Fetch all raw sectioned data items in :mod:`dict`.

        .. note:: *DEFAULT* section is not included as it's not in sections() call.
        """
        attr = '_rawdata'
        if self.dirty or hasattr(self, attr) is False:
            settings = dict([(sec, dict(self.parser.items(sec))) for sec in self.parser.sections()])
            setattr(self, attr, settings)
            self.dirty = False
        return getattr(self, attr)


    @property
    def data(self):
        """Fetch all sectioned data item in :mod:`dict` with auto type-aware checking.

        """
        return self.rawdata


    def get(self, section, option, callback=None):
        """Return value of the :data:`option` under :data:`section`.

        Args:
            * section: config section.
            * option: config option under section.
            * callback: additional filter for the returned value, default is :data:`None`.
        """
        value = self.parser.get(section, option)
        return callable(callback) and callback(value) or value


    def has_section(self, section, func=None):
        """Check if the :data:`section` is already exist if configuration.
    
        .. todo:: func: custom method for matching section, :data:`None` by default.

        Args:
            * section: full section name.

        """
        if func:
            pass
        return self.parser.has_section(section)


    def reload(self):
        """Reload settings from the created path.

        """
        if os.path.exists(self.path) is False:
            raise IOError('<path: {path}> not found'.format(path=self.path))

        if not self.parser.read(self.path):
            raise IOError('<path: {path}> is not a valid config file.'.format(path=self.path))
        self.dirty = True


    def remove_section(self, section):
        """Remove :data:`section` from in-memory config & flush back to the file.

        :param str section: config section.

        :return: :data:`True` if the section is successfully removed from config file.
        """
        # TODO more safe IO error handling.
        with open(self.path, 'w') as fileobj:
            self.parser.remove_section(section)
            self.parser.write(fileobj)
            self.reload()
        return True
