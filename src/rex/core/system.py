#!/usr/bin/env python
# -*- coding: utf-8 -*-
r"""
======================================================
:mod:`system` --- system related constants. 
======================================================

"""
import os
import shlex
import subprocess

from sirocco.core import decos
from sirocco.core import regex
from sirocco.core.logger import getLogger


__all__ = ['abspath', 'join', 'parent']


logger = getLogger(__name__)


# ============================== Lambda Helpers ==============================
Dir = lambda path: path.startswith('~') \
        and os.path.expanduser(path) \
        or (path.startswith('./') and path[2:] or path)


#==========================================================================================
#   General/Common Properties 
#==========================================================================================
root    = os.path.abspath('/')
userdir = os.path.expanduser('~')


# ============================== system functions ==============================
def abspath(path=os.path.curdir, dirname=False):
    """Create an absolute path and check its validity.

    Shortcut to :func:`os.path.abspath` with userdir supports.

    Args:
        * path: pathname, default is current working directory.
        * dirname: whether to return absolute directory path only instead of including the file (if exists).

    Returns:
        Absolute version of :data:`path`.
    """
    abspath = os.path.abspath(Dir(path))
    return (dirname and os.path.isfile(abspath)) and os.path.dirname(abspath) or abspath


def join(*args):
    """Create a relative path and check its validity.

    Shortcut to :func:`os.path.join` with userdir & directory's validity supports.

    Args:
        * args: directories to be joined.

    Returns:
        Relative joint path.
    """
    cwd, dirs = os.path.curdir, list(args)
    if not args: return cwd
    return os.path.join(cwd, *list(map(Dir, dirs))) 


def parent(path=os.path.curdir):
    """Return absolute parent path of the given :data:`path`.

    In addition to constructing the parent path, also validate 
    the constructed value see if it's valid, parent path with 
    the same value as the given :data:`path` will be considered 
    as invalid.

    Args:
        * path: base path to check, default is current working directory.
    """
    parent = abspath(join(path, os.path.pardir))
    return (os.path.exists(parent) and parent != abspath(path)) and parent or None


@decos.memorize
def find(pattern, path=os.path.curdir, recursive=False):
    """Find all matched item by walking through from :data:`root`.

    Args:
        * pattern: search pattern, support both string and :mod:`re` pattern .
        * path: root path to start searching, default is current working directory.
        * recursive: whether to recursively find the matched items from :data:`path`, :data:`False` by default.

    Returns
        :mod:`list` of absolute path and isfile flag in :class:`tuple`.
    """
    result, basepath = [], abspath(path)
    Finder = lambda item: regex.is_regex(pattern) and pattern.match(item) or (pattern == item)

    if recursive is True:
        for base, dirs, files in os.walk(basepath, topdown=True):
            # python3 does not return list by filter directly as python2 does.
            matched = list(filter(Finder, files)) + list(filter(Finder, dirs))
            for item in matched:
                itempath = join(base, item)
                result.append((itempath, os.path.isfile(itempath)))
    else:
        items = filter(Finder, listdir(basepath))
        for item in items:
            itempath = join(basepath, item)
            result.append((itempath, os.path.isfile(itempath)))
    return result


def listdir(path=os.curdir, callback=None):
    """List files & diectories under the given path.

    By default, this method will list all item under :data:`path`.
    
    Args:
        * path: path to list, default is current working directory.
        * callback: additional function for filtering the result list.

    Returns: 
        Files/diectories list under the given path in :mod:`list`.
    """
    items = os.listdir(abspath(path))
    return callable(callback) and list(filter(callback, items)) or items


def mkdir(name, path=os.path.curdir):
    """Create directory under the given :data:`path`.
    
    Args:
        * name: directory name to create.
        * path: base path for creating directories.

    Returns:
        Absolute path of the directory if it's successfully created, :data:`None` otherwise.
    """
    target, created = abspath(path, name), False
    if os.path.isdir(target):
        logger.warn('<dir: {dir}> is already exists'.format(dir=target))
        return target, False
    os.mkdir(target)
    created = True
    logger.debug('<dir: {dir}> created'.format(dir=name))
    return target, created


def execute(*commands, **kwargs):
    """Run the system command with optional params.

    Args:
        * commands: va-list of string-based commands.

    Kwargs:
        * verbose: direct options for :func:`subprocess.Popen`.
    """
    verbose = kwargs.pop('verbose', False)
    # Ensure we are on the very base working directory before actually doing anything by specifying cwd.
    kwargs['cwd'] = kwargs.pop('cwd', os.path.curdir)
    #kwargs['stdout'] = kwargs.pop('stdout', subprocess.PIPE)
    #kwargs['stderr'] = kwargs.pop('stderr', subprocess.PIPE)
    for command in commands:
        result = subprocess.Popen(shlex.split(command), **kwargs)
        stdout, stderr = result.communicate()
        if verbose is True:
            logger.debug('<command: {command}>'.format(command=command))
            if stdout: logger.debug(stdout)
            if stderr: logger.error(stderr)

