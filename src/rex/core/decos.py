# -*- coding: utf-8 -*-
r"""
:mod:`decos` --- decorator tool set. 
==========================================


"""
from decorator import decorator
from timeit import default_timer as timer

from rex.core import logger as Logger


__all__ = ('decorator', 'timeit',)


logger = Logger.getLogger(__name__)


@decorator
def timeit(func, *args, **kwargs):
    """Logging the time for :data:`func`.

    Decorator for method/function to caculate the time it used
    and add the logs to :mod:`logging`.
    """
    start = timer()
    result = func(*args, **kwargs)
    cost = timer() - start
    logger.debug('<method: %s> finished in %2.2f sec' % (func.__name__, cost))
    return result


__cache__ = {}

@decorator
def memorize(func, *args, **kwargs):
    """Simply memorize the calculated result :data:`func`. previously returned.

    Simply cached all calculated results from the decorated method/function into
    a global :data:`dict`.
    """

    if (len(args) > 0 and len(kwargs) > 0):
        cacheKey = list(args)
        cacheKey.append(kwargs)
    elif (len(args) > 0):
        cacheKey = args
    else:
        cacheKey = func.__name__
    
    global __caches__
    result = __cache__.get(cacheKey)
    if result is None:
        result = func(*args, **kwargs)
        __cache__[cacheKey] = result
    return result
